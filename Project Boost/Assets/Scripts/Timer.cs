using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
public class Timer : MonoBehaviour
{
    private TextMeshProUGUI boxText;
    // Start is called before the first frame update
    void Start()
    {
        boxText = gameObject.GetComponent<TextMeshProUGUI>();        

    }

    // Update is called once per frame
    void Update()
    {
        boxText.text = "Time: " + ((int)Time.timeSinceLevelLoad);
    }
}
