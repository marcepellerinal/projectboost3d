using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FuelBox : MonoBehaviour
{
    [SerializeField] GameObject HudObject;
    [SerializeField] GameObject LightFace;
    [SerializeField] GameObject LightUp;
    [SerializeField] AudioClip _audioClip;
    [SerializeField] GameObject RocketObject;

    private RectTransform _rectTransform;
    private MeshRenderer _MeshRendereCube;
    private CanvasRenderer _CanvasRenderer;
    private AudioSource _audioSource;
    private bool withoutFuel = false;
    //private Movement _movement;


    // Start is called before the first frame update
    void Start()
    {
        _rectTransform = HudObject.GetComponent<RectTransform>();
        _MeshRendereCube = this.gameObject.GetComponent<MeshRenderer>();
        _CanvasRenderer = HudObject.GetComponent<CanvasRenderer>();
        _audioSource = gameObject.GetComponent<AudioSource>();
        _audioSource.clip = _audioClip;
        //_movement = RocketObject.GetComponent<Movement>();
    }

    // Update is called once per frame
    void Update()
    {
        if(_rectTransform.sizeDelta.x <= 100f && _rectTransform.sizeDelta.x > 20f) 
        {   
            if (withoutFuel)
            {
                Debug.Log("Menor o igual que 100");
                _CanvasRenderer.SetColor(Color.red);
                withoutFuel = false;
                RocketObject.GetComponent<Movement>().enabled = true;
            }           
        }
        else if (_rectTransform.sizeDelta.x > 100f)
        {       
                if(withoutFuel)
                { 
                   Debug.Log("Mayor que 100");
                _CanvasRenderer.SetColor(Color.white);
                withoutFuel = false;
                RocketObject.GetComponent<Movement>().enabled = true;
                }
        }
        else if (_rectTransform.sizeDelta.x <= 20f) 
        {
            if (!withoutFuel) 
            {
                withoutFuel = true;
                Debug.Log("Menor o igual que 0");
                RocketObject.GetComponent<Movement>().enabled = false;
                RocketObject.GetComponent<Movement>().StopThrusting();
                RocketObject.GetComponent<Movement>().StopRotating();
            }
        }

        if(((int)Time.time)%2 == 0)
        {
            Debug.Log("Bool: " + withoutFuel);
            Debug.Log("Width: " + _rectTransform.sizeDelta.x);
        }  
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            StartCoroutine("DissFuelBox");
            _rectTransform.sizeDelta = new Vector2(400f, 100f);
        }
    }

    private IEnumerator DissFuelBox()
    {
        _audioSource.Play();
        _MeshRendereCube.material.color = Color.red;
        yield return new WaitForSeconds(0.1f);
        _MeshRendereCube.material.color = Color.white;
        yield return new WaitForSeconds(0.1f);
        _MeshRendereCube.material.color = Color.red;
        yield return new WaitForSeconds(0.1f);
        this.gameObject.SetActive(false);
        LightFace.SetActive(false);
        LightUp.SetActive(false);
        yield return new WaitForSeconds(0.8f);
        _audioSource.Stop();
        yield return null;
    }

}
