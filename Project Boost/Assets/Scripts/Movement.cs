using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour
{

    [SerializeField] float mainThrust = 100f;
    [SerializeField] float rotationThrust = 2f;
    [SerializeField] AudioClip mainEngine;
    [SerializeField] GameObject HudMenu;

    [SerializeField] ParticleSystem MainBooster;
    [SerializeField] ParticleSystem RightBooster;
    [SerializeField] ParticleSystem LeftBooster;
    [SerializeField] float wasteOfFuel = 0.005f;

    private RectTransform _rectTransform;
    private Rigidbody _rigidbody;
    private AudioSource _audiosource;

    // Start is called before the first frame update
    void Start()
    {
        _rigidbody = gameObject.GetComponent<Rigidbody>();
        _audiosource = gameObject.GetComponent<AudioSource>();
        _rectTransform = HudMenu.GetComponent<RectTransform>();
    }

    // Update is called once per frame
    void Update()
    {
        ProccesThrust();
        ProcesRotation();
        
    }

    void ProccesThrust()
    {
        if (Input.GetKey(KeyCode.Space))
        {
            StartThrusting();
        }
        else
        {
            StopThrusting();
        }

    }

    void ProcesRotation()
    {
        if (Input.GetKey(KeyCode.A))
        {
            RotateLeft();
        }
        else if (Input.GetKey(KeyCode.D))
        {
            RotateRight();
        }
        else
        {
            StopRotating();
        }
    }

    private void StartThrusting()
    {
        _rigidbody.AddRelativeForce(Vector3.up * Time.deltaTime * mainThrust);
        _rectTransform.sizeDelta = new Vector2(_rectTransform.sizeDelta.x * (1f-wasteOfFuel), _rectTransform.sizeDelta.y);
        if (!_audiosource.isPlaying)
        {
            _audiosource.PlayOneShot(mainEngine);
        }
        if (!MainBooster.isPlaying && !RightBooster.isPlaying && !LeftBooster.isPlaying)
        {
            MainBooster.Play();
        }
    }

    public void StopThrusting()
    {
        MainBooster.Stop();
        if (_audiosource.isPlaying && !Input.GetKey(KeyCode.D) && !Input.GetKey(KeyCode.A))
        {
            _audiosource.Stop();
        }
    }

    private void RotateLeft()
    {
        ApplyRotation(rotationThrust);
        if (!RightBooster.isPlaying)
        {
            RightBooster.Play();
        }
        if (!_audiosource.isPlaying)
        {
            _audiosource.PlayOneShot(mainEngine);
        }
    }

    private void RotateRight()
    {
        ApplyRotation(-rotationThrust);
        if (!LeftBooster.isPlaying)
        {
            LeftBooster.Play();
        }
        if (!_audiosource.isPlaying)
        {
            _audiosource.PlayOneShot(mainEngine);
        }
    }

    public void StopRotating()
    {
        RightBooster.Stop();
        LeftBooster.Stop();
        if (_audiosource.isPlaying && !Input.GetKey(KeyCode.Space))
        {
            _audiosource.Stop();
        }
    }

    void ApplyRotation(float rotationThisFrame)
    {
        _rigidbody.freezeRotation = true;
        transform.Rotate(Vector3.forward * rotationThisFrame * Time.deltaTime);
        _rigidbody.freezeRotation = false;
    }

}
