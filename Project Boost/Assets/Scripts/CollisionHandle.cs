using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CollisionHandle : MonoBehaviour
{
    [SerializeField] float LevelLoadDelay = 1f;
    [SerializeField] AudioClip crash;
    [SerializeField] AudioClip success;
    [SerializeField] ParticleSystem successParticles;
    [SerializeField] ParticleSystem crashParticles;



    AudioSource audiosource;

    bool isTransitioning = false;
    bool collisionDisable = false;

     void Start()
    {
        audiosource = gameObject.GetComponent<AudioSource>();
    }

    void Update()
    {
        RespondToDebugKeys();
    }

    private void OnCollisionEnter(Collision collision)
    {   if(isTransitioning || collisionDisable)
        {
            return;
        }
            switch (collision.gameObject.tag)
            {
                case "Friendly":
                    break;
                case "Finish":               
                        StartSuccessSequence();
                    break;
                default:
                    //if (!collision.gameObject.CompareTag("BoundingCamera")
                    //{
                        StartCrashSequence();
                    //}              
                    break;
            }
    }
    public bool GetIsTransitioning()
    {
        return this.isTransitioning;
    }

    void StartSuccessSequence()
    {
        isTransitioning = true;
        audiosource.Stop();    
        audiosource.PlayOneShot(success);
        gameObject.GetComponent<Movement>().enabled = false;
        successParticles.Play();    
        Invoke("LoadNextLevel", LevelLoadDelay);
    }
    void StartCrashSequence()
    {
        isTransitioning = true;
        audiosource.Stop();
        audiosource.PlayOneShot(crash);
        gameObject.GetComponent<Movement>().enabled = false;
        crashParticles.Play();
        Invoke("ReloadLevel", LevelLoadDelay);
    }
    void ReloadLevel()
    {
        int currentSceneIndex = SceneManager.GetActiveScene().buildIndex;
        SceneManager.LoadScene(currentSceneIndex);
    }

    void LoadNextLevel()
    {
        int currentSceneIndex = SceneManager.GetActiveScene().buildIndex;
        int nextLevelScene = currentSceneIndex + 1;
        if(nextLevelScene == SceneManager.sceneCountInBuildSettings)
        {
            nextLevelScene = 0;
        }
        SceneManager.LoadScene(nextLevelScene);
    }

    void RespondToDebugKeys()
    {
        if (Input.GetKeyDown(KeyCode.L))
        {
            LoadNextLevel();
        }
        else if (Input.GetKeyDown(KeyCode.C))
        {
            collisionDisable = !collisionDisable;
        }
    }
}
